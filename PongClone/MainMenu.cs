﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace MonoPong
{
    public class MainMenu : IControllable, IDrawable
    {
        private string[] options;
        private int selectionIndex = 0;
        private App game;

        private bool isDownKeyDown = false;
        private bool isUpKeyDown = false;

        public MainMenu(App game)
        {
            this.game = game;
            options = new string[]
            {
                "Player vs Player",
                "AI vs AI",
                "Player vs AI Easy",
                "Player vs AI Medium",
                "Player vs AI Hard",
                "Quit"
            };
        }

        public void Draw()
        {
            GameUtils.DrawRect(0, 0, GameUtils.FieldWidth, GameUtils.FieldHeight, new Color(Color.Black, 0.8f));
            for (int i = 0; i < options.Length; i++)
            {
                Color color = (i == selectionIndex) ? Color.White : Color.Blue;
                GameUtils.SpriteBatch.DrawString(GameUtils.MediumFont, options[i], 
                    new Vector2((GameUtils.FieldWidth - 22 * options[i].Length) / 2 + 22, 150 + 60 * i), 
                    color);
            }
        }

        public void HandleKeys()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Up) && !isUpKeyDown)
            {
                selectionIndex--;

                if (selectionIndex < 0)
                {
                    selectionIndex = options.Length - 1;
                }

                isUpKeyDown = true;
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Down) && !isDownKeyDown)
            {
                selectionIndex++;

                if (selectionIndex > options.Length - 1)
                {
                    selectionIndex = 0;
                }

                isDownKeyDown = true;
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                Ball ball = new Ball();
                Pad pad1 = new PadAi(ball);
                Pad pad2 = new PadAi(ball);

                int positionX1 = 50;
                int positionX2 = GameUtils.FieldWidth - Pad.Width - positionX1;

                switch (options[selectionIndex])
                {
                    case "Player vs Player":
                        pad1 = new PadPlayer(ball, Keys.W, Keys.S, positionX1, 0);
                        pad2 = new PadPlayer(ball, Keys.Up, Keys.Down, positionX2, 0);
                        break;
                    case "AI vs AI":
                        pad1 = new PadAi(ball, Keys.W, Keys.S, positionX1, 0);
                        pad2 = new PadAi(ball, Keys.Up, Keys.Down, positionX2, 0);
                        pad1.Awareness = 0.8f;
                        pad2.Awareness = 0.8f;
                        break;
                    case "Player vs AI Easy":
                        pad1 = new PadPlayer(ball, Keys.W, Keys.S, positionX1, 0);
                        pad2 = new PadAi(ball, Keys.Up, Keys.Down, positionX2, 0);
                        pad2.Awareness = 0.35f;
                        break;
                    case "Player vs AI Medium":
                        pad1 = new PadPlayer(ball, Keys.W, Keys.S, positionX1, 0);
                        pad2 = new PadAi(ball, Keys.Up, Keys.Down, positionX2, 0);
                        pad2.Awareness = 0.6f;
                        break;
                    case "Player vs AI Hard":
                        pad1 = new PadPlayer(ball, Keys.W, Keys.S, positionX1, 0);
                        pad2 = new PadAi(ball, Keys.Up, Keys.Down, positionX2, 0);
                        pad2.Awareness = 0.85f;
                        break;
                    case "Quit":
                        game.Exit();
                        break;
                }

                game.IsPlaying = true;
                game.InitGame(ball, pad1, pad2);
            }

            if (Keyboard.GetState().IsKeyUp(Keys.Up))
            {
                isUpKeyDown = false;
            }
            if (Keyboard.GetState().IsKeyUp(Keys.Down))
            {
                isDownKeyDown = false;
            }
        }
    }
}
