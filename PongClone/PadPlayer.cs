﻿using Microsoft.Xna.Framework.Input;

namespace MonoPong
{
    public class PadPlayer : Pad
    {
        public PadPlayer(Ball ball) : base(ball) { }

        public PadPlayer(Ball ball, Keys keyUp, Keys keyDown) : base(ball, keyUp, keyDown) { }

        public PadPlayer(Ball ball, Keys keyUp, Keys keyDown, int x, int y) : base(ball, keyUp, keyDown, x, y) { }

        public override void HandleKeys()
        {
            if (Keyboard.GetState().IsKeyDown(keyUp))
            {
                Move(Direction.Up);
            }
            else if (Keyboard.GetState().IsKeyDown(keyDown))
            {
                Move(Direction.Down);
            }
            else
            {
                Brake();
            }
        }
    }
}
