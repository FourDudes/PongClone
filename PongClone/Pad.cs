﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;

namespace MonoPong
{
    public abstract class Pad : IControllable, IDrawable, IVeloMoveable, IResetable
    {
        public const int Width = 20;
        public const int Height = 90;
        private const float velocityMax = 7f;
        private const float velocityDelta = 0.5f;
        private const float velocityBreakFactor = 0.9f;

        public float Awareness = 0f;
        public Vector2 Position;
        protected Ball ball;
        protected Keys keyUp, keyDown;
        private Vector2 velocity;

        public Pad(Ball ball) : this(ball, Keys.D0, Keys.D0) { }

        public Pad(Ball ball, Keys keyUp, Keys keyDown) : this(ball, Keys.D0, Keys.D0, 0, 0) { }

        public Pad(Ball ball, Keys keyUp, Keys keyDown, int x, int y)
        {
            this.ball = ball;
            Position = new Vector2(x, y);
            velocity = new Vector2(0, 0);
            this.keyUp = keyUp;
            this.keyDown = keyDown;
        }

        public abstract void HandleKeys();

        public void Draw()
        {
            GameUtils.DrawRect(Position.X, Position.Y, Width, Height, Color.White);
        }

        public void UpdatePos()
        {
            Position.X += velocity.X;
            Position.Y += velocity.Y;

            if (Position.Y <= GameUtils.UiHeight)
            {
                Position.Y = GameUtils.UiHeight;
                velocity.Y *= -1;
            }

            if (Position.Y + Height >= GameUtils.FieldHeight)
            {
                Position.Y = GameUtils.FieldHeight - Height;
                velocity.Y *= -1;
            }
        }

        protected void Move(Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    velocity.Y -= velocityDelta;
                    break;
                case Direction.Down:
                    velocity.Y += velocityDelta;
                    break;
            }

            if (velocity.Y > velocityMax || velocity.Y < -velocityMax)
            {
                velocity.Y = Math.Sign(velocity.Y) * velocityMax;
            }
        }

        protected void Brake()
        {
            velocity.Y *= velocityBreakFactor;
        }

        public void Reset()
        {
            Position.Y = (GameUtils.FieldHeight - GameUtils.UiHeight + Height) / 2;
            velocity.Y = 0f;
        }
    }
}
