﻿using Microsoft.Xna.Framework;
using System;

namespace MonoPong
{
    public class Ball : IDrawable, IVeloMoveable, IResetable
    {
        public const int Width = 15;
        public const int Height = 15;

        private const float defaultSpeed = 7f;
        private const float verticalMaxSpeed = 5f;
        private const float speedDelta = 0.12f;

        public Vector2 Position;
        private Vector2 velocity;

        public Ball() : this(0, 0) { }

        public Ball(int x, int y)
        {
            Position = new Vector2(x, y);
            velocity = new Vector2(0, 0);
        }

        public void Draw()
        {
            GameUtils.DrawRect(Position.X, Position.Y, Width, Height, Color.White);
        }

        public void UpdatePos()
        {
            Position.X += velocity.X;
            Position.Y += velocity.Y;

            if (Position.Y <= GameUtils.UiHeight)
            {
                Position.Y = GameUtils.UiHeight;
                velocity.Y *= -1;
            }

            if (Position.Y + Height >= GameUtils.FieldHeight)
            {
                Position.Y = GameUtils.FieldHeight - Height;
                velocity.Y *= -1;
            }
        }

        public void Collide(Pad pad)
        {
            if (Position.Y + Height >= pad.Position.Y
                && Position.Y <= pad.Position.Y + Pad.Height
                && Position.X + Width >= pad.Position.X
                && Position.X <= pad.Position.X + Pad.Width)
            {
                float ballCenter = Position.Y + Height / 2f;
                float padCenter = pad.Position.Y + Pad.Height / 2f;

                float verticalVelocity = (ballCenter - padCenter) / (Pad.Height / 2f);

                // Left side.
                if (Position.X + Width <= pad.Position.X + Math.Abs(velocity.X))
                {
                    Position.X = pad.Position.X - Width;
                    velocity.Y = verticalVelocity * verticalMaxSpeed;
                    velocity.X *= -1;
                    velocity.X += Math.Sign(velocity.X) * speedDelta;
                }
                // Right side.
                if (Position.X >= pad.Position.X + Pad.Width - Math.Abs(velocity.X))
                {
                    Position.X = pad.Position.X + Pad.Width;
                    velocity.Y = verticalVelocity * verticalMaxSpeed;
                    velocity.X *= -1;
                    velocity.X += Math.Sign(velocity.X) * speedDelta;
                }
                // Top/bottom sides.
                if (Position.Y + Height <= pad.Position.Y + Math.Abs(velocity.Y)
                    || Position.Y >= pad.Position.Y + Pad.Height - Math.Abs(velocity.Y))
                {
                    velocity.Y *= -1;
                }
            }
        }

        public void Reset()
        {
            velocity.X = Math.Sign(velocity.X) * defaultSpeed;
            velocity.Y = Math.Sign(velocity.Y) * verticalMaxSpeed * (float) GameUtils.rand.NextDouble();

            Position.X = (GameUtils.FieldWidth + Width) / 2;
            Position.Y = (GameUtils.FieldHeight + GameUtils.UiHeight - Height) / 2;
        }

        public void RandomizeDirection()
        {
            velocity.X = defaultSpeed * Math.Sign(GameUtils.rand.NextDouble() - 0.5);
            velocity.Y = verticalMaxSpeed * (float)(GameUtils.rand.NextDouble() - 0.5) * 2f; 
        }

        public void DrawDirectionLine()
        {
            GameUtils.DrawLine(
                new Vector2(Position.X + Width / 2, Position.Y + Height / 2),
                new Vector2(
                    Position.X + velocity.X * GameUtils.DirectionLineMultiplier,
                    Position.Y + velocity.Y * GameUtils.DirectionLineMultiplier),
                GameUtils.DirectionLineColor, 2f);
        }
    }
}
