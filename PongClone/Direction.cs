﻿namespace MonoPong
{
    public enum Direction
    {
        Up, Down, Left, Right
    }
}
