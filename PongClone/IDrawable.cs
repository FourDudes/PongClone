﻿namespace MonoPong
{
    interface IDrawable
    {
        void Draw();
    }
}
