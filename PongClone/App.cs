﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;

namespace MonoPong
{
    public class App : Game
    {
        private bool isCounting = true;
        private bool isPlaying = false;

        private int counter = GameUtils.CounterAtStart;
        private int score1 = 0, score2 = 0;

        private Starline[] starlines;
        private MainMenu menu;
        private Stopwatch stopwatch;
        private Pad pad1, pad2;
        private Ball ball;

        public bool IsPlaying { get => isPlaying; set => isPlaying = value; }

        public App()
        {
            IsMouseVisible = true;
            GameUtils.Graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = GameUtils.FieldWidth,
                PreferredBackBufferHeight = GameUtils.FieldHeight
            };
            Content.RootDirectory = "Content";

            menu = new MainMenu(this);
            stopwatch = new Stopwatch();
            ball = new Ball();
            pad1 = new PadAi(ball, Keys.W, Keys.S, 50, 0);
            pad2 = new PadAi(ball, Keys.Up, Keys.Down, GameUtils.FieldWidth - 50 - Pad.Width, 0);

            starlines = new Starline[GameUtils.AmountOfStarlines];

            for (int i = 0; i < starlines.Length; i++)
            {
                starlines[i] = new Starline();
            }

            pad1.Awareness = 0.65f;
            pad2.Awareness = 0.65f;

            InitGame(ball, pad1, pad2);
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            GameUtils.SpriteBatch = new SpriteBatch(GraphicsDevice);
            GameUtils.MediumFont = Content.Load<SpriteFont>("MediumFont");
        }

        public void InitGame(Ball ball, Pad pad1, Pad pad2)
        {
            this.pad1 = pad1;
            this.pad2 = pad2;
            this.ball = ball;

            score1 = 0;
            score2 = 0;
            ball.RandomizeDirection();
            ResetRound();
        }

        private void ResetRound()
        {
            pad1.Reset();
            pad2.Reset();
            ball.Reset();

            isCounting = true;
            counter = GameUtils.CounterAtStart;
            stopwatch.Start();
        }

        protected override void UnloadContent() { }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                Exit();
            }

            if (!isCounting)
            {
                pad1.HandleKeys();
                pad2.HandleKeys();

                pad1.UpdatePos();
                pad2.UpdatePos();
                ball.UpdatePos();

                ball.Collide(pad1);
                ball.Collide(pad2);

                ScorePoint();
            }

            if (IsPlaying && Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                IsPlaying = false;
            }

            if (!IsPlaying)
            {
                menu.HandleKeys();
            }

            foreach (Starline starline in starlines)
            {
                starline.Update();
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            GameUtils.SpriteBatch.Begin();

            foreach (Starline starline in starlines)
            {
                starline.Draw();
            }

            GameUtils.DrawRect(0, GameUtils.UiHeight - GameUtils.UiBarHeight,
                GameUtils.FieldWidth, GameUtils.UiBarHeight, Color.White);

            GameUtils.SpriteBatch.DrawString(GameUtils.MediumFont, "" + score1,
                new Vector2(150, 20), Color.White);
            GameUtils.SpriteBatch.DrawString(GameUtils.MediumFont, "" + score2,
                new Vector2(GameUtils.FieldWidth - 175, 20), Color.White);

            pad1.Draw();
            pad2.Draw();
            
            if (isCounting)
            {
                GameUtils.SpriteBatch.DrawString(GameUtils.MediumFont, counter + "...",
                    new Vector2(GameUtils.FieldWidth / 2, 200), Color.White);
                ball.DrawDirectionLine();

                if (stopwatch.ElapsedMilliseconds >= 1000)
                {
                    counter--;
                    stopwatch.Restart();
                }

                if (counter == 0)
                {
                    isCounting = false;
                }
            }

            ball.Draw();

            if (!IsPlaying)
            {
                menu.Draw();
            }

            GameUtils.SpriteBatch.End();
            base.Draw(gameTime);
        }

        private void ScorePoint()
        {
            if (ball.Position.X + Ball.Width < 0)
            {
                score2++;
                ResetRound();
            }
            else if (ball.Position.X > GameUtils.FieldWidth)
            {
                score1++;
                ResetRound();
            }
        }
    }
}
