﻿using Microsoft.Xna.Framework.Input;
using System;

namespace MonoPong
{
    public class PadAi : Pad
    {
        public PadAi(Ball ball) : base(ball) { }

        public PadAi(Ball ball, Keys keyUp, Keys keyDown) : base(ball, keyUp, keyDown) { }

        public PadAi(Ball ball, Keys keyUp, Keys keyDown, int x, int y) : base(ball, keyUp, keyDown, x, y) { }

        public override void HandleKeys()
        {
            float range = Math.Abs((ball.Position.X + Ball.Width / 2) - (Position.X + Width / 2)) / GameUtils.FieldWidth;

            if (range < Awareness)
            {
                if (ball.Position.Y + Ball.Height / 2 < Position.Y + Awareness * Ball.Height * 2)
                {
                    Move(Direction.Up);
                }
                else if (ball.Position.Y + Ball.Height / 2 > Position.Y - Awareness * Ball.Height * 2)
                {
                    Move(Direction.Down);
                }
                else
                {
                    Brake();
                }
            }
            else
            {
                Brake();
            }
        }
    }
}
