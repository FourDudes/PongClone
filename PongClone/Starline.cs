﻿using Microsoft.Xna.Framework;
using System;

namespace MonoPong
{
    public class Starline : IDrawable
    {
        private static Vector2 screenCenter = new Vector2(
            GameUtils.FieldWidth / 2,
            (GameUtils.FieldHeight + GameUtils.UiHeight) / 2);
        private const float movementMultiplier = 1.1f;
        private Vector2 point1;
        private Vector2 point2;

        public Starline()
        {
            Init();
        }

        private void Init()
        {
            point1.X = (float) (GameUtils.rand.NextDouble() - 0.5) * 6f;
            point1.Y = (float) (GameUtils.rand.NextDouble() - 0.5) * 6f;

            point2.X = point1.X * 3f;
            point2.Y = point1.Y * 3f;
        }

        public void Update()
        {
            point1.X *= movementMultiplier;
            point1.Y *= movementMultiplier;
            point2.X *= movementMultiplier;
            point2.Y *= movementMultiplier;

            if (point1.X < -screenCenter.X || point1.Y < -screenCenter.Y
                || point1.X > GameUtils.FieldWidth || point1.Y > GameUtils.FieldHeight)
            {
                Init();
            }
        }

        public void Draw()
        {
            Vector2 p1 = new Vector2(point1.X + screenCenter.X, point1.Y + screenCenter.Y);
            Vector2 p2 = new Vector2(point2.X + screenCenter.X, point2.Y + screenCenter.Y);

            GameUtils.DrawLine(p1, p2, new Color(20, 20, 20), 1);
        }
    }
}
