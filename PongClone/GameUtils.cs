﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace MonoPong
{
    public static class GameUtils
    {
        public const int FieldWidth = 800;
        public const int FieldHeight = 600;

        public const int AmountOfStarlines = 50;

        public const int UiHeight = 100;
        public const int UiBarHeight = 5;

        public const int CounterAtStart = 3;

        public static Color DirectionLineColor = Color.Orange;
        public const float DirectionLineMultiplier = 15f;

        public static SpriteBatch SpriteBatch;
        public static GraphicsDeviceManager Graphics;
        public static SpriteFont MediumFont;
        public static Random rand = new Random();
        private static Texture2D rectTexture;

        public static void DrawRect(float x, float y, int width, int height, Color color)
        {
            if (rectTexture == null)
            {
                rectTexture = new Texture2D(Graphics.GraphicsDevice, 1, 1);
                rectTexture.SetData(new[] { Color.White });
            }
            SpriteBatch.Draw(rectTexture, new Rectangle((int) x, (int) y, width, height), color);
        }

        public static void DrawLine(Vector2 point1, Vector2 point2, Color color, float thickness = 1f)
        {
            var distance = Vector2.Distance(point1, point2);
            var angle = (float) Math.Atan2(point2.Y - point1.Y, point2.X - point1.X);
            DrawLine(point1, distance, angle, color, thickness);
        }

        public static void DrawLine(Vector2 point, float length, float angle, Color color, float thickness = 1f)
        {
            if (rectTexture == null)
            {
                rectTexture = new Texture2D(Graphics.GraphicsDevice, 1, 1);
                rectTexture.SetData(new[] { Color.White });
            }

            var origin = new Vector2(0f, 0.5f);
            var scale = new Vector2(length, thickness);
            SpriteBatch.Draw(rectTexture, point, null, color, angle, origin, scale, SpriteEffects.None, 0);
        }
    }
}
